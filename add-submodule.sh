#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

while [[ true ]]; do
	echo ""
	echo ""
	echo "====== add submodule ======"
	echo ""
	echo ""

	printf "Enter git repo: "
	read GIT_PATH_TO_REPO

	printf "Enter checkout branch: "
	read GIT_BRANCH

	printf "Enter local path: "
	read LOCAL_PATH

	git submodule add -b $GIT_BRANCH $GIT_PATH_TO_REPO $LOCAL_PATH
	git submodule init
done
