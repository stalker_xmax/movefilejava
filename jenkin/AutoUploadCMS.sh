#!/bin/bash -x

#Init Global Params
PROJECT_OTA="$PROJECT_OTA"
BUILD_NUMBER="${BUILD_NUMBER}"

#PROJECT_OTA="32.Dota_Ayakashi2"
#BUILD_NUMBER="123456"

SHELL_LOCATION="/Users/build/Documents/JenkinsAutoBuild/AutoUploadCMS"
MOUNT_PATH="${SHELL_LOCATION}/MountOTA/${PROJECT_OTA}"
FILESERVER_OTA_PATH="192.168.30.112/Game_Artist_Ope/Project/${PROJECT_OTA}/UploadCMS"
SOURCE_PATH="${MOUNT_PATH}/Input"
DESTINATION_PATH="${MOUNT_PATH}/Output"
EXCEL_FILE_PATH="${SOURCE_PATH}/input.xlsx"
OUTPUT_FOLDER=""
SUB_SHORT_DATE_FOLDER=""
PROJECT_API="${PROJECT_API}"
UPLOAD_TYPE="${UPLOAD_TYPE}"
ZIP_FILE="${ZIP_FILE}"
RESPONSE=""
JSON_PATH=""
UPLOAD_ERROR=""

SHORT_DATE=$(date "+%Y-%m-%d")
SHORT_TIME=$(date "+%H-%M")

UPLOAD_FILE_LOCATION="/Users/build/Documents/jenkins_newbuilder/workspace/APP_BUILDER_AutoUploadCMS/default"


#test
#umount -f ${MOUNT_PATH}
#exit 1

CheckOutPutPath(){
	OUTPUT_FOLDER="${BUILD_NUMBER}_${SHORT_TIME}"
	SUB_SHORT_DATE_FOLDER="${DESTINATION_PATH}/${SHORT_DATE}"
	OUTPUT_PATH="${SUB_SHORT_DATE_FOLDER}/${OUTPUT_FOLDER}"
		
	if test -d "${OUTPUT_PATH}"; then
		echo "exist output folder"
	else
		mkdir -p "${OUTPUT_PATH}"
	fi
}

ZipAllFile(){	
	cd $SUB_SHORT_DATE_FOLDER
	if [[ -d $OUTPUT_FOLDER ]]; then		

		cd $OUTPUT_FOLDER
		zip -r "${OUTPUT_FOLDER}.zip" *
		cp  "${OUTPUT_FOLDER}.zip" $SUB_SHORT_DATE_FOLDER	

		cd $SUB_SHORT_DATE_FOLDER 
		rm -rf $OUTPUT_FOLDER/		
	fi
}

RemoveAllFile(){
	cd $SOURCE_PATH
	for f in *; do       	
	   	rm -rf $f	    
	done
}

ReadJsonFile() {
  key=$2
  value=$(cat $1 | sed 's/{.*\'$key'":"*\([0-9a-zA-Z|.|_|-|\/]*\)"*,*.*}/\1/') 
  echo $value
}

ZipFileProgress(){
	cd $SOURCE_PATH
	if [[ -f "${SOURCE_PATH}/input.xlsx" ]]; then
		echo "exist input.xlsx"
	else
		echo "Error: input.xlsx file not exist"
		umount -f ${MOUNT_PATH}
		exit 1
	fi

	CheckOutPutPath

	cd $SHELL_LOCATION

	java -jar MoveFile.jar $EXCEL_FILE_PATH $SOURCE_PATH $OUTPUT_PATH

	sleep 1

	ZipAllFile

	cd $SOURCE_PATH
	cp "input.xlsx" $SUB_SHORT_DATE_FOLDER
	cd $SUB_SHORT_DATE_FOLDER
	mv "input.xlsx" "${OUTPUT_FOLDER}_input.xlsx"

	RemoveAllFile

	# umount -f ${MOUNT_PATH}

	echo "progress zip file is completed"
}

WriteJsonFile(){
JSON_PATH="$MOUNT_PATH/Logs/${BUILD_NUMBER}_response.json"
cat <<EOF> $JSON_PATH
	${RESPONSE}
EOF

	#check for error
	UPLOAD_ERROR=$(cat $JSON_PATH | grep 'errorFiles')	
}

# CheckErrorResponse(){
# 	#foo=$(ReadJsonFile $JSON_PATH "errorFiles")
# 	#echo $foo | awk -F '"errorFiles": \\[ ' '{print $2}' | awk -F ' \\], "msg"' '{print $1}'
# }

UploadAfterZipProgress(){
	echo "POST......"
	FILE_TO_POST_PATH="${MOUNT_PATH}/Output/${SHORT_DATE}/${OUTPUT_FOLDER}.zip"
	echo $FILE_TO_POST_PATH

	RESPONSE=$(curl -F "file=@${FILE_TO_POST_PATH}" $PROJECT_API)
	WriteJsonFile

	echo "progress upload after zip is completed"
}

UploadAlreadyZipProgress(){
	echo "POST......"

	if [[ -z "$ZIP_FILE" ]]; then
		echo "zip file not exist"
		exit 1
	fi

	cd $UPLOAD_FILE_LOCATION
	mv "ZIP_FILE" "${ZIP_FILE}.zip"
	cd SHELL_LOCATION

	FILE_TO_POST_PATH="${UPLOAD_FILE_LOCATION}/${ZIP_FILE}.zip"
	echo $FILE_TO_POST_PATH

	RESPONSE=$(curl -F "file=@${FILE_TO_POST_PATH}" $PROJECT_API)
	WriteJsonFile

	echo "progress upload already zip is completed"
}

#mount to sync data to server
USERNAME="bm1"
PASSWORD="Unity_2015"
echo $FILESERVER_OTA_PATH
echo $MOUNT_PATH
mount_smbfs smb://$USERNAME:$PASSWORD@${FILESERVER_OTA_PATH} ${MOUNT_PATH}

if [[ $UPLOAD_TYPE == "ZipAndUpload" ]]; then
	echo "ZipAndUpload"
	ZipFileProgress
	UploadAfterZipProgress
	
	echo "ZipAndUpload is completed"
else
	if [[ $UPLOAD_TYPE == "Zip" ]]; then
		echo "Zip only"
		ZipFileProgress

		echo "Zip only is completed"
	else
		echo "upload only"
		UploadAlreadyZipProgress
		echo "Upload only is completed"
	fi
fi

#umount after done
umount -f ${MOUNT_PATH}

if [[ $UPLOAD_ERROR != "" ]]; then
	echo "FAIL"
	exit 1
else
	echo "SUCCESS"
fi

exit