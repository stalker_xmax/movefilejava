#!/bin/bash -x

#Init Global Params

LAST_COMMIT=""
BUILD_TARGET=""
BUILD_NEW_METHOD="${BUILD_NEW_METHOD}"
GIT_RESET_HARD="${GIT_RESET_HARD}"
TEXTURE_COMPRESS="${TEXTURE_COMPRESS}"
TEXTURE_RESIZE="${TEXTURE_RESIZE}"
PROJECT_ROOT_FOLDER="$PROJECT_ROOT_FOLDER"

BUILD_TARTGETS="$BUILD_TARTGETS"
SVN_USERNAME="${SVN_USERNAME}"
SVN_PASSWORD="${SVN_PASSWORD}"

SHELL_LOCATION="/Users/build/Documents/JenkinsAutoBuild/AssetBundle_AutoBuild"

PROJECT_NAME="$PROJECT_NAME"
BRANCH="$BRANCH"
UNITY_VERSION="$UNITY_VERSION"

PROJECT_LOCATION=""
UNITY_PROJECT_PATH=""
UNITY_LOCATION="/Applications/${UNITY_VERSION}/Unity.app/Contents/MacOS/Unity"


DATE=$(date "+%Y%m%d%H%M%S")
TIME=$(date "+%Y-%m-%d : %H-%M-%S")
SHORT_DATE=$(date "+%Y-%m-%d")
SHORT_TIME=$(date "+%H-%M")
LOG_PATH="${SHELL_LOCATION}/LOGS/log_AB_ADR_${BUILD_NUMBER}_${DATE}.txt"

PROJECT_OTA="$PROJECT_OTA"
MOUNT_PATH="${SHELL_LOCATION}/MountOTA/${PROJECT_OTA}"
SOURCE_PATH=""

#server path
UPLOAD_FILE_NAME="INPUT_FILE"
UPLOAD_FILE_LOCATION="/Users/build/Documents/jenkins_newbuilder/workspace/APP_BUILDER_AssetBundle/default"
FILESERVER_OTA_PATH="192.168.30.112/Game_Artist_Ope/Project/${PROJECT_OTA}/ExportAssetBundle"

#split params into array
PARAMS=($(echo "$BUILD_TARTGETS" | tr ',' '\n'))

#test
#umount -f ${MOUNT_PATH}
#exit 1

OUTPUT_PATH=""

#Execution

StartBuildAssetBundle() {	
	PROJECT_LOCATION="${SHELL_LOCATION}/AssetBundle/${BUILD_TARGET}/${PROJECT_NAME}/${BRANCH}"
	UNITY_PROJECT_PATH="${PROJECT_LOCATION}/${PROJECT_ROOT_FOLDER}"
	SOURCE_PATH="${UNITY_PROJECT_PATH}/AssetBundles"
	
	
	if [[ -d "${PROJECT_LOCATION}/.git" ]]; then
		git -C "${PROJECT_LOCATION}" reset --hard origin/$BRANCH
		git -C "${PROJECT_LOCATION}" clean -f
		git -C "${PROJECT_LOCATION}" pull origin $BRANCH
		git -C "${PROJECT_LOCATION}" checkout $BRANCH
		LAST_COMMIT=$(git -C "${PROJECT_LOCATION}" log -n 1)
		echo "Build with Git"
	else
		SVN_OPTION="--non-interactive --no-auth-cache"
		svn cleanup $PROJECT_LOCATION $SVN_OPTION --username "$SVN_USERNAME" --password "$SVN_PASSWORD"
		svn up $PROJECT_LOCATION $SVN_OPTION --username "$SVN_USERNAME" --password "$SVN_PASSWORD" --accept tf
		svn revert $PROJECT_LOCATION --recursive -R $SVN_OPTION --username "$SVN_USERNAME" --password "$SVN_PASSWORD"
		LAST_COMMIT=$?
		echo "Build with Svn"
	fi	
	
	echo $LAST_COMMIT		
	
	echo $UNITY_PROJECT_PATH

	sleep 2

	#Unity BatchMode Build
	echo "Unity Building Asset bundle..."
	"$UNITY_LOCATION" -batchmode -nographics -projectPath $UNITY_PROJECT_PATH -executeMethod AssetBundleAutoBuild.Init -quit -CustomArgs:"INPUT_PATH=${INPUT_FILE_LOCATION}@BUILD_TARGET=${BUILD_TARGET}@BUILD_NEW_METHOD=${BUILD_NEW_METHOD}@TEXTURE_COMPRESS=${TEXTURE_COMPRESS}@TEXTURE_RESIZE=${TEXTURE_RESIZE}" -logFile $LOG_PATH
	
	sleep 1	
	echo "Unity build complete!"
}

MoveFile(){	
	if test -d "${SOURCE_PATH}"; then		
		#PLATFORM=$BUILD_TARGET		
		#tr '[A-Z]' '[a-z]' < $PLATFORM

		CheckOutPutPath

		echo "move file to mount location"
		cp -a ${SOURCE_PATH}/* ${OUTPUT_PATH}
		rm -rf ${SOURCE_PATH}/*
		
	else
		echo "${SOURCE_PATH} not exist, build error"
		exit 1
	fi
}

USERNAME="bm1"
PASSWORD="Unity_2015"
mount_smbfs smb://$USERNAME:$PASSWORD@${FILESERVER_OTA_PATH} ${MOUNT_PATH}

CheckOutPutPath(){
	OUTPUT_PATH="${MOUNT_PATH}/${SHORT_DATE}/${BUILD_NUMBER}_${SHORT_TIME}"
		
	if test -d "${OUTPUT_PATH}"; then
		echo "exist mount folder"
	else
		mkdir -p "${OUTPUT_PATH}"
	fi
}

CheckOutPutPath

#rename & copy input file to backup for trace log purpose
UPLOAD_FILE_NAME="${BUILD_NUMBER}_${INPUT_FILE}"
echo $UPLOAD_FILE_NAME
cd $UPLOAD_FILE_LOCATION
mv "INPUT_FILE" "${UPLOAD_FILE_NAME}"
cp "${UPLOAD_FILE_NAME}" $OUTPUT_PATH
echo "copy ${INPUT_FILE} to ${OUTPUT_PATH}"

INPUT_FILE_LOCATION="${UPLOAD_FILE_LOCATION}/${UPLOAD_FILE_NAME}"

echo "===== INPUT_FILE_LOCATION ${INPUT_FILE_LOCATION}"

#build all platform
for PARAM in "${PARAMS[@]}"
    do
		#BUILD_TARGET=$PARAM
		BUILD_TARGET=$(echo ${PARAM} | tr '[:upper:]' '[:lower:]')
		echo "======>>>Build for ${BUILD_TARGET}"
 		StartBuildAssetBundle
 		sleep 1
		MoveFile
		sleep 5
done

#zip all output files
cd ${MOUNT_PATH}

ZipAllFile(){
	for f in *; do
	    if [[ -d $f ]]; then
	        "${f} is a directory"

	       	cd ${MOUNT_PATH}/$f
	       	zip -r "${f}.zip" *

	       	cp -a $f.zip ${MOUNT_PATH}
		   	rm -rf $f.zip

	       	cd ${MOUNT_PATH}
	       	rm -rf $f/
	    fi
	done
}

#dont need zip file now
#ZipAllFile

umount -f ${MOUNT_PATH}

echo 'Build Asset Bundle DONE!'
exit