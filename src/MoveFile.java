
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

public class MoveFile
{
  static String excelFileName = getPathJarFile() + "excel.xlsx";
  static String source = getPathJarFile() + "source";
  static String destination = getPathJarFile() + "destination";


  static String basePath = getPathJarFile();

  public static void main(String[] args) throws Exception
  {
    if (args.length > 0) {
      excelFileName = args[0];
      source = args[1];
      destination = args[2];
    }
    File file = new File(excelFileName);
    readExcelData(file);
  }

  public static String getPathJarFile() {
    URL url = MoveFile.class.getProtectionDomain().getCodeSource().getLocation();
    String jarPath = null;
    try {
      jarPath = URLDecoder.decode(url.getFile(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    String parentPath = new File(jarPath).getParentFile().getPath();
    parentPath = parentPath + File.separator;

    System.out.println("Path: " + parentPath);
    return parentPath;
  }

  public static void readExcelData(File file) throws Exception {
    FileInputStream excel = null;

    String fileName = file.getAbsolutePath();
    excel = new FileInputStream(file);


    Workbook workbook = null;
    if (fileName.toLowerCase().endsWith("xlsx")) {
      workbook = new XSSFWorkbook(excel);
    } else if (fileName.toLowerCase().endsWith("xls")) {
      workbook = new HSSFWorkbook(excel);
    }

    if (workbook == null) {
      return;
    }
    Sheet worksheet = workbook.getSheetAt(0);
    FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
    Iterator<Row> rowIterator = worksheet.iterator();
    Map<String, String> mapFile = GetFilesSource();
    boolean ignoreFirstRow = true;
    while (rowIterator.hasNext()) {
      Row row = (Row)rowIterator.next();
      if (ignoreFirstRow){
        ignoreFirstRow = false;
        continue;
      }

      String key = readCell(row.getCell(0), evaluator);
      String value = readCell(row.getCell(1), evaluator);
      String type = readCell(row.getCell(2), evaluator);
      String subType = readCell(row.getCell(3), evaluator);
      String volume = readCell(row.getCell(4), evaluator);
      String sync = readCell(row.getCell(5), evaluator);
      String version = readCell(row.getCell(6), evaluator);
      String activeVersion = readCell(row.getCell(7), evaluator);

      if (mapFile.get(key) != null) {
        key = (String)mapFile.get(key);
      }

      if ((key != null) && (!key.isEmpty())
          && (value != null) && (!value.isEmpty())
          && type != null && !type.isEmpty()
          && subType != null && !subType.isEmpty()) {
        String sourcePath = source + File.separator + key + File.separator;
        String destinationPath = destination + File.separator + value + File.separator;
        copyFiles(sourcePath, destinationPath);

        String[] arr = key.split("\\.");
        fileName = arr.length > 0 ? arr[0] : key;

        try {
          String jsonString = new JSONObject()
              .put("name", fileName)
              .put("type", type)
              .put("volume", volume)
              .put("sync", sync)
              .put("version", version)
              .put("activeVersion", activeVersion)
              .put("subType", subType).toString();

          FileWriter fileWriter = new FileWriter(destinationPath + File.separator + "meta.json");
          fileWriter.write(jsonString);
          fileWriter.flush();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static Map<String, String> GetFilesSource() {
    Map<String, String> result = new HashMap();
    String sourcePath = basePath + source + File.separator;
    File dir = new File(sourcePath);
    if (!dir.exists()) {
      return result;
    }
    File[] filesInDir = dir.listFiles();
    File[] arrayOfFile1;
    int j = (arrayOfFile1 = filesInDir).length; for (int i = 0; i < j; i++) { File file = arrayOfFile1[i];
      if (file.isFile()) {
        String name = file.getName();
        String key = name.substring(0, name.lastIndexOf("."));
        result.put(key, name);
        System.out.println("name " + name + " key " + key);
      }
    }
    return result;
  }

  public static void copyFiles(String sourcePath, String destinationPath) {
    try {
      File dir = new File(sourcePath);
      File destination = new File(destinationPath);
      if (!destination.exists()) {
        destination.mkdirs();
      }
      if (!dir.exists()) {
        System.out.println("Source path not exist " + dir.getPath());
        return;
      }
      if (dir.isFile()) {
        copyFiles(sourcePath, destinationPath, dir);
      } else {
        File[] filesInDir = dir.listFiles();
        File[] arrayOfFile1; int j = (arrayOfFile1 = filesInDir).length; for (int i = 0; i < j; i++) { File file = arrayOfFile1[i];
          copyFiles(sourcePath, destinationPath, file);
        }
      }
    }
    catch (Exception e) {
      System.out.println(String.format("Error move file from %s to %s", new Object[] { sourcePath, destinationPath }));
      e.printStackTrace();
    }
  }

  private static void copyFiles(String sourcePath, String destinationPath, File file) throws IOException {
    String name = file.getName();

    copyFileUsingChannel(file, new File(destinationPath + name));
    System.out.println(String.format("Move file %s from %s to %s", new Object[] { name, sourcePath, destinationPath }));
  }

  private static void copyFileUsingChannel(File source, File dest)
    throws IOException
  {
    FileChannel inputChannel = null;
    FileChannel outputChannel = null;
    try {
      inputChannel = new FileInputStream(source).getChannel();
      outputChannel = new FileOutputStream(dest).getChannel();
      outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
    } finally {
      inputChannel.close();
      outputChannel.close();
    }

  }

  public static String readCell(Cell cell, FormulaEvaluator evaluator)
    throws Exception
  {
    String val = null;
    if (cell == null) {
      return null;
    }
    switch (cell.getCellType()) {
    case 1:
      val = cell.getStringCellValue();
      break;
    case 0:
      double number = cell.getNumericCellValue();
      int intvalue = (int)number;
      val = String.valueOf(intvalue);
      break;
    case 4:
      break;
    case 2:
      val = evaluator.evaluate(cell).formatAsString().replaceAll("\"", "");
      break;
    case 3:
      break;
    case 5:
      break;
    }


    return val;
  }
}


/* Location:              C:\Users\tri.nn\Desktop\Chuyen Folder_ID\moveFile.jar!\movefile\MoveFile.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */